package main;

import main.Main;
import java.util.*;

public class Script
{
	public int activeNPC = 0;
	public ArrayList<NPC> NPCs = new ArrayList<NPC>();

	public static Script newScript()
	{
		Script script = new Script();
		script.NPCs.add(NPC.newNPC());
		return script;
	}
	public String makeScript()
	{
		String script = "";
		for(NPC npc : NPCs)
		{
			script += "#" + Main.configuration.mobj.get(npc.actor) + "\n";
			script += npc.makeScript();
		}
		return script;
	}
	public String makeUSDF()
	{
		String script = "namespace = \"Strife\";\ninclude = \"SCRIPT00\";\n\n";
		for(NPC npc: NPCs)
		{
			script += "//" + Main.configuration.mobj.get(npc.actor) + "\n";
			script += npc.makeUSDF();
		}
		return script;
	}
}