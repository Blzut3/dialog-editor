package main;

public class DialogChoice
{
	public int give = -1;
	private String text = "";
	public int log = 0;
	private String yes = "";
	private String no = "";
	public int cost = 0; //mobj
	public int cost_check = -1; //amount
	public boolean noDisplayCost = false;
	public int altcost = -1;
	public int altcost_check = 0;
	public int link = 0;
	public boolean showNext = false;

	public String makeScript()
	{
		String script = "\n	CHOICE\n";
		if(give != -1)
		{
			script += "		GIVE " + give + "\n";
		}
		script += "		TEXT \"" + text + "\"\n";
		if(log != 0)
		{
			script += "		LOG " + log + "\n";
		}
		if(yes.equals("") == false)
		{
			script += "		YES \"" + yes + "\"\n";
		}
		if(no.equals("") == false)
		{
			script += "		NO \"" + no + "\"\n";
		}
		if(cost != -1 && !noDisplayCost)
		{
			script += "		COST " + cost + " * " + cost_check + "\n";
		}
		else if(cost != -1 && noDisplayCost)
		{
			script += "		NODISPLAYCOST " + cost + " * " + cost_check + "\n";
		}
		if(altcost != -1)
		{
			script += "		ALTCOST " + altcost + " * " + altcost_check + "\n";
		}
		script += "		LINK ";
		if(showNext)
		{
			script += "-";
		}
		script += (link + 1) + "\n";
		script += "	;\n";
		return script;
	}
	public String makeUSDF()
	{
		String script = "\t\tchoice\n\t\t{\n\t\t\ttext = \"" + text + "\";\n";
		if(cost != -1)
			script += "\t\t\tcost\n\t\t\t{\n\t\t\t\titem = " + cost + ";\n\t\t\t\tamount = " + cost_check + ";\n\t\t\t}\n";
		if(altcost != -1)
			script += "\t\t\tcost\n\t\t\t{\n\t\t\t\titem = " + altcost + ";\n\t\t\t\tamount = " + altcost_check + ";\n\t\t\t}\n";
		if(noDisplayCost)
			script += "\t\t\tdisplaycost = false;\n";
		if(yes.equals("") == false)
			script += "\t\t\tyesmessage = \"" + yes + "\";\n";
		if(no.equals("") == false)
			script += "\t\t\tnomessage = \"" + no + "\";\n";
		if(log != 0)
			script += "\t\t\tlog = \"LOG" + log + "\";\n";
		if(give != -1)
			script += "\t\t\tgiveitem = " + give + ";\n";
		script += "\t\t\tnextpage = " + (link+1) + ";\n";
		if(!showNext)
			script += "\t\t\tclosedialog = true;\n";
		script += "\t\t}\n";
		return script;
	}
	public String toString() //toString() returns what ZDoom/Strife would print
	{
		String returnString = getText();
		if(cost != -1 && !noDisplayCost)
		{
			returnString += " for " + cost_check;
		}
		return returnString;
	}
	public String getText()
	{
		return text;
	}
	public void setText(String text)
	{
		if(text.length() <= 32)
		{
			this.text = text;
		}
		else
		{
			this.text = text.substring(0, 32);
		}
	}
	public String getYes()
	{
		return yes;
	}
	public void setYes(String text)
	{
		if(text.length() <= 80)
		{
			yes = text;
		}
		else
		{
			yes = text.substring(0, 80);
		}
	}
	public String getNo()
	{
		return no;
	}
	public void setNo(String text)
	{
		if(text.length() <= 80)
		{
			no = text;
		}
		else
		{
			no = text.substring(0, 80);
		}
	}
}