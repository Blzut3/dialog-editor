package main;

import main.Main;

import java.io.*;
import java.util.ArrayList;

public class Config
{
	public String iwad = "";
	public String pwad = "";
	public String font = "";
	public int[] translation = {0,0,0};
	public int[] translation_green = {0,0,0};
	public int[] translation_white = {0,0,0};
	public ArrayList<String> mobj = new ArrayList<String>();

	public Config(String config)
	{
		try
		{
			BufferedReader in = new BufferedReader(new FileReader(config));
			String line = null;
			while((line = in.readLine()) != null)
			{
				if(line.indexOf("//") != -1)
				{
					line = line.substring(0, line.indexOf("//"));
				}
				if(line.indexOf("=") != -1)
				{
					String setting = line.substring(0, line.indexOf("=")).trim();
					String value = line.substring(line.indexOf("=")+1).trim();
					parseSetting(setting, value);
				}
			}
			in.close();
		}
		catch(Exception e) //fatal error
		{
			Main.fatalError(e, "Could not read the config file.");
		}
	}

	public void readConfigLump(short[] data)
	{
		try
		{
			int pos = 0;
			while(pos != data.length)
			{
				String line = null;
				StringBuffer lineReader = new StringBuffer();
				while(pos != data.length) //read until end of line or end of file
				{
					if(data[pos] == 10)
					{
						pos++;
						break;
					}
					lineReader.append((char) data[pos]);
					pos++;
				}
				line = lineReader.toString();
				if(line.indexOf("//") != -1)
				{
					line = line.substring(0, line.indexOf("//"));
				}
				if(line.indexOf("=") != -1)
				{
					String setting = line.substring(0, line.indexOf("=")).trim();
					String value = line.substring(line.indexOf("=")+1).trim();
					parseSetting(setting, value);
				}
			}
		}
		catch(Exception e)
		{
			Main.fatalError(e, "Could not read the config lump.");
		}
	}
	private void parseSetting(String setting, String value) throws Exception
	{
		if(setting.equals("iwad"))
		{
			iwad = value;
		}
		else if(setting.equals("pwad"))
		{
			pwad = value;
		}
		else if(setting.equals("font"))
		{
			font = value;
		}
		else if(setting.equals("font_remap_default"))
		{
			translation[0] = Integer.parseInt(value.substring(0, value.indexOf(":")));
			translation[1] = Integer.parseInt(value.substring(value.indexOf(":")+1, value.indexOf(",")));
			translation[2] = Integer.parseInt(value.substring(value.indexOf(",")+1).trim());
		}
		else if(setting.equals("font_remap_green"))
		{
			translation_green[0] = Integer.parseInt(value.substring(0, value.indexOf(":")));
			translation_green[1] = Integer.parseInt(value.substring(value.indexOf(":")+1, value.indexOf(",")));
			translation_green[2] = Integer.parseInt(value.substring(value.indexOf(",")+1).trim());
		}
		else if(setting.equals("font_remap_white"))
		{
			translation_white[0] = Integer.parseInt(value.substring(0, value.indexOf(":")));
			translation_white[1] = Integer.parseInt(value.substring(value.indexOf(":")+1, value.indexOf(",")));
			translation_white[2] = Integer.parseInt(value.substring(value.indexOf(",")+1).trim());
		}
		else if(setting.equals("mobj"))
		{
			int index = -2;
			if(value.indexOf(",") != -1)
			{
				try
				{
					index = Integer.parseInt(value.substring(0, value.indexOf(",")).trim());
					if(index < -1)
					{
						index = -2;
					}
					else
					{
						value = value.substring(value.indexOf(",")+1).trim();
					}
				}
				catch(NumberFormatException e) {} //ignore
			}
			if(value.equals("<Unkown Mobj>")) //don't know why anyone would do this...
			{
				value = "<Unkown Mobj> ";
			}
			if(index == -1)
			{
				mobj.add(mobj.size() + " " + value);
			}
			else if(index < mobj.size() && index != -2)
			{
				mobj.set(index, index + " " + value);
			}
			else if(index == mobj.size() || index == -2)
			{
				mobj.add(value);
			}
			else
			{
				for(int i = mobj.size();i < index;i++)
				{
					mobj.add(i + " " + "<Unkown Mobj>");
				}
				mobj.add(index + " " + value);
			}
		}
	}
}