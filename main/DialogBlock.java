package main;

import main.*;

import java.util.ArrayList;

public class DialogBlock
{
	public int drop = -1;
	public int[] if_item = {-1, -1, -1};
	public int if_item_goto = 0; //GOTO
	private String name = "";
	private String panel = "";
	private String voice = "";
	private String dialog = "";
	public ArrayList<DialogChoice> choices = new ArrayList<DialogChoice>();

	public void setName(String name)
	{
		if(name.length() <= 16)
		{
			this.name = name;
		}
	}
	public void setPanel(String panel)
	{
		if(panel.length() <= 8)
		{
			this.panel = panel;
		}
		else
		{
			this.panel = panel.substring(0, 8);
		}
	}
	public void setDialog(String dialog)
	{
		if(name.length() <= 320)
		{
			this.dialog = dialog;
		}
	}
	public void setVoice(String voice)
	{
		if(voice.length() <= 8)
		{
			this.voice = voice;
		}
		else
		{
			this.voice = voice.substring(0, 8);
		}
	}
	public String getName()
	{
		return name;
	}
	public String getPanel()
	{
		return panel;
	}
	public String getDialog()
	{
		return dialog;
	}
	public String getVoice()
	{
		return voice;
	}
	public String makeScript(int mobj)
	{
		String script = "ACTOR " + mobj + "\n";
		if(drop != -1)
		{
			script += "	DROP " + drop + "\n";
		}
		if(if_item[0] != -1)
		{
			script += "	IF_ITEM " + if_item[0] + "\n";
			if(if_item[1] != -1)
			{
				script += "	IF_ITEM2 " + if_item[1] + "\n";
			}
			if(if_item[2] != -1)
			{
				script += "	IF_ITEM3 " + if_item[2] + "\n";
			}
			script += "	GOTO " + if_item_goto + "\n";
		}
		script += "	NAME \"" + getName() + "\"\n";
		if(getVoice().equals("") == false)
		{
			script += "	VOICE \"" + getVoice() + "\"\n";
		}
		if(getPanel().equals("") == false)
		{
			script += "	PANEL \"" + getPanel() + "\"\n";
		}
		script += "	DIALOG \"" + getDialog() + "\"\n";
		if(choices.size() > 0)
		{
			for(DialogChoice choice : choices)
			{
				script += choice.makeScript();
			}
		}
		script += "END\n\n";
		return script;
	}
	public String makeUSDF()
	{
		String script = "\tpage\n\t{\n\t\tname = \"" + getName() + "\";\n";
		if(getPanel().equals("") == false)
			script += "\t\tpanel = \"" + getPanel() + "\";\n";
		if(getVoice().equals("") == false)
			script += "\t\tvoice = \"" + getVoice() + "\";\n";
		script += "\t\tdialog = \"" + getDialog() + "\";\n";
		if(drop != -1)
			script += "\t\tdrop = " + drop + ";\n";
		for(int i = 0;i < 3;i++)
		{
			if(if_item[i] != -1)
			{
				script += "\t\tifitem\n\t\t{\n\t\t\titem = " + if_item[i] + "\n\t\t\tamount = 1;\n\t\t\tpage = " + if_item_goto + ";\n\t\t}\n";
			}
		}
		if(choices.size() > 0)
		{
			for(DialogChoice choice : choices)
			{
				script += choice.makeUSDF();
			}
		}
		script += "\t}\n";
		return script;
	}
}