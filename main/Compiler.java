package main;

import main.*;

import java.io.*;

public class Compiler
{
	public static void compile(File file)
	{
		try
		{
			FileOutputStream out = new FileOutputStream(file);
			for(NPC npc : Main.script.NPCs)
			{
				int actor = npc.actor;
				for(DialogBlock page : npc.pages)
				{
					out.write(putInteger(actor));
					out.write(putInteger(page.drop+1));
					out.write(putInteger(page.if_item[0]+1));
					out.write(putInteger(page.if_item[1]+1));
					out.write(putInteger(page.if_item[2]+1));
					out.write(putInteger(page.if_item_goto));
					out.write(putString(page.getName(), 16));
					out.write(putString(page.getVoice(), 8));
					out.write(putString(page.getPanel(), 8));
					out.write(putString(page.getDialog(), 320));
					for(int i = 0;i < 5;i++)
					{
						DialogChoice choice = new DialogChoice();
						if(i < page.choices.size())
						{
							choice = page.choices.get(i);
						}
						else
						{
							byte[] dummy = new byte[228];
							out.write(dummy);
							continue;
						}
						//prepare the link value
						int link = choice.link+1;
						if(choice.showNext)
						{
							link *= -1;
						}
						//now write the choice
						int give = choice.give;
						int cost = choice.cost;
						int altcost = choice.altcost;
						if(choice.give == -1)
							give = 0;
						out.write(putInteger(give));
						if(choice.cost == -1)
							cost = 0;
						if(choice.altcost == -1)
							altcost = 0;
						if(choice.noDisplayCost)
						{
							out.write(putInteger(cost));
							out.write(putInteger(0));
							out.write(putInteger(altcost));
							out.write(putInteger(choice.cost_check));
							out.write(putInteger(0));
							out.write(putInteger(choice.altcost_check));
						}
						else
						{
							out.write(putInteger(0));
							out.write(putInteger(cost));
							out.write(putInteger(altcost));
							out.write(putInteger(0));
							out.write(putInteger(choice.cost_check));
							out.write(putInteger(choice.altcost_check));
						}
						out.write(putString(choice.getText(), 32));
						out.write(putString(choice.getYes(), 80));
						out.write(putSignedInteger(link));
						out.write(putInteger(choice.log));
						out.write(putString(choice.getNo(), 80));
					}
				}
			}
			out.close();
		}
		catch(Exception e)
		{
			Main.error(e, "Could not write compiled output.");
		}
	}
	private static byte[] putInteger(long in)
	{
		String str = Long.toHexString(in);
		while(str.length() < 8)
			str = "0" + str;
		byte[] value = {
			(byte) Integer.parseInt(str.substring(6, 8), 16),
			(byte) Integer.parseInt(str.substring(4, 6), 16),
			(byte) Integer.parseInt(str.substring(2, 4), 16),
			(byte) Integer.parseInt(str.substring(0, 2), 16)
		};
		return value;
	}
	private static byte[] putSignedInteger(int in)
	{
		long out = in;
		if(in < 0)
		{
			out = (long) Integer.MAX_VALUE*2;
			out = out + (long) (in+2);
		}
		return putInteger(out);
	}
	private static byte[] putString(String in, int length)
	{
		char[] str = in.toCharArray();
		byte[] out = new byte[length];
		for(int i = 0;i < str.length;i++)
		{
			out[i] = (byte) str[i];
		}
		return out;
	}
}