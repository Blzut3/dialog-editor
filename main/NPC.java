package main;

import java.util.*;
import javax.swing.*;

public class NPC
{
	public int actor = 0;
	public ArrayList<DialogBlock> pages = new ArrayList<DialogBlock>();
	private SpinnerNumberModel[] blocks = {
		new SpinnerNumberModel(0, 0, 0, 1), //page #
		new SpinnerNumberModel(0, 0, 0, 1), //if item
		new SpinnerNumberModel(0, 0, 0, 1) //link
	};

	public SpinnerNumberModel getPagesModel()
	{
		return getPagesModel(0);
	}
	public SpinnerNumberModel getPagesModel(int num)
	{
		return blocks[num];
	}
	public void updateModels()
	{
		blocks[0] = new SpinnerNumberModel(0, 0, pages.size()-1, 1);
		if(Main.page != null)
		{
			if(Main.page.if_item_goto > pages.size()-1)
			{
				Main.page.if_item_goto = 0;
			}
			blocks[1] = new SpinnerNumberModel(Main.page.if_item_goto, 0, pages.size()-1, 1);
		}
		else
		{
			if(pages.get(0).if_item_goto > pages.size()-1)
			{
				pages.get(0).if_item_goto = 0;
			}
			blocks[1] = new SpinnerNumberModel(pages.get(0).if_item_goto, 0, pages.size()-1, 1);
		}
		blocks[2] = new SpinnerNumberModel(0, 0, pages.size()-1, 1);
		Main.gui.pageSpinner.setModel(getPagesModel());
		Main.gui.pageSpinner_ifItem.setModel(getPagesModel(1));
		Main.gui.pageSpinner_link.setModel(getPagesModel(2));
	}
	public DialogBlock newPage()
	{
		DialogBlock page = new DialogBlock();
		pages.add(page);
		Main.setPage(page);
		blocks[0] = new SpinnerNumberModel(pages.size()-1, 0, pages.size()-1, 1);
		blocks[1] = new SpinnerNumberModel(Main.page.if_item_goto, 0, pages.size()-1, 1);
		blocks[2] = new SpinnerNumberModel(0, 0, pages.size()-1, 1);
		Main.gui.pageSpinner.setModel(getPagesModel());
		Main.gui.pageSpinner_ifItem.setModel(getPagesModel(1));
		Main.gui.pageSpinner_link.setModel(getPagesModel(2));
		return page;
	}
	public void deletePage(DialogBlock page)
	{
		if(pages.contains(page) == false || pages.size() == 1)
		{
			return;
		}
		int index = pages.indexOf(page);
		pages.remove(index);
		if(index != 0)
		{
			index--;
		}
		Main.setPage(pages.get(index));
		blocks[0] = new SpinnerNumberModel(index, 0, pages.size()-1, 1);
		if(Main.page.if_item_goto != 0 && Main.page.if_item_goto == index+1)
		{
			Main.page.if_item_goto--;
		}
		blocks[1] = new SpinnerNumberModel(Main.page.if_item_goto, 0, pages.size()-1, 1);
		blocks[2] = new SpinnerNumberModel(0, 0, pages.size()-1, 1);
		Main.gui.pageSpinner.setModel(getPagesModel());
		Main.gui.pageSpinner_ifItem.setModel(getPagesModel(1));
		Main.gui.pageSpinner_link.setModel(getPagesModel(2));
	}
	public String makeScript()
	{
		String script = "";
		for(DialogBlock page : pages)
		{
			script += page.makeScript(actor);
		}
		return script;
	}
	public String makeUSDF()
	{
		String script = "conversation\n{\n\tactor = " + actor + ";\n";
		for(DialogBlock page : pages)
		{
			script += page.makeUSDF();
		}
		script += "}\n\n";
		return script;
	}
	public static NPC newNPC()
	{
		NPC npc = new NPC();
		npc.newPage();
		return npc;
	}
}