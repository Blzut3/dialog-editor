package main;

import main.*;

import java.io.*;

public class Decompiler
{
	static final int BLOCK_SIZE = 1516;

	public static Script decompile(File file)
	{
		try
		{
			byte[] buffer = new byte[(int) file.length()];
			short[] data = new short[buffer.length];
			FileInputStream in = new FileInputStream(file);
			in.read(buffer, 0, buffer.length);
			in.close();
			for(int i = 0;i < buffer.length;i++)
			{
				if(buffer[i] < 0)
				{
					data[i] = (short) (256 + buffer[i]);
				}
				else
				{
					data[i] = buffer[i];
				}
			}
			return decompile(data);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	public static Script decompile(short[] data)
	{
		if(data.length%BLOCK_SIZE != 0)
			return null; //invalid file size
		int blocks = data.length/BLOCK_SIZE;
		Script script = new Script();
		NPC npc = null;
		for(int i = 0;i < blocks;i++)
		{
			int offset = i*BLOCK_SIZE;
			int actor = getInteger(data, offset);
			int drop = getInteger(data, offset+4);
			int[] if_item = {getInteger(data, offset+8), getInteger(data, offset+12), getInteger(data, offset+16)};
			int if_item_goto = getInteger(data, offset+20);
			String name = getString(data, offset+24, 16).trim();
			String voice = getString(data, offset+40, 8).trim();
			String panel = getString(data, offset+48, 8).trim();
			String dialog = getString(data, offset+56, 320).trim();
			if(npc == null || npc.actor != actor)
			{
				if(npc != null && npc.pages.size() != 0)
				{
					npc.updateModels();
					script.NPCs.add(npc);
				}
				npc = new NPC();
				npc.actor = actor;
			}
			DialogBlock page = new DialogBlock();
			page.drop = drop;
			page.if_item = if_item;
			page.if_item_goto = if_item_goto;
			page.setName(name);
			page.setVoice(voice);
			page.setPanel(panel);
			page.setDialog(dialog);
			offset += 376;
			for(int j = 0;j < 5;j++)
			{
				if(getString(data, offset+28, 32).trim().equals(""))
					continue; //nothing to see here move on
				int give = getInteger(data, offset);
				int cost = getInteger(data, offset+4);
				int cost_amount = getInteger(data, offset+16);
				boolean noDisplayCost = false;
				if(cost == 0)
				{
					noDisplayCost = true;
				}
				if(getInteger(data, offset+8) != 0)
				{
					noDisplayCost = true;
					cost = getInteger(data, offset+8);
					cost_amount = getInteger(data, offset+20);
				}
				int altCost = getInteger(data, offset+12);
				int altCost_amount = getInteger(data, offset+24);
				String text = getString(data, offset+28, 32).trim();
				String yes = getString(data, offset+60, 80).trim();
				int link = getSignedInteger(data, offset+140);
				int log = getInteger(data, offset+144);
				String no = getString(data, offset+148, 80).trim();
				DialogChoice choice = new DialogChoice();
				choice.give = give;
				choice.cost = cost;
				choice.cost_check = cost_amount;
				choice.noDisplayCost = noDisplayCost;
				choice.altcost = altCost;
				choice.altcost_check = altCost_amount;
				choice.setText(text);
				choice.setYes(yes);
				if(link > 0)
				{
					choice.showNext = false;
					choice.link = link-1;
				}
				else if(link == 0)
				{
					choice.showNext = false;
					choice.link = 0;
				}
				else
				{
					choice.showNext = true;
					choice.link = (-link)-1;
				}
				choice.log = log;
				choice.setNo(no);
				page.choices.add(choice);
				offset += 228;
			}
			npc.pages.add(page);
		}
		if(npc != null) //finish adding npcs
		{
			script.NPCs.add(npc);
		}
		return script;
	}
	private static int getInteger(short[] data, int offset)
	{
		return data[offset] + data[offset+1]*0x100 + data[offset+2]*0x10000 + data[offset+3]*0x1000000;
	}
	private static int getSignedInteger(short[] data, int offset)
	{
		int value = getInteger(data, offset);
		if(value > 2147483647)
		{
			value = -(value - 2147483647);
		}
		return value;
	}
	private static String getString(short[] data, int offset, int length)
	{
		char[] str = new char[length];
		for(int i = 0;i < length;i++)
			str[i] = (char) data[offset+i];
		return new String(str);
	}
}