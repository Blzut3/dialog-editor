package gui;

import main.*;
import gui.*;
import wad.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;

public class GUI extends WindowAdapter implements ActionListener, CaretListener, ChangeListener, ItemListener, WindowListener
{
	boolean previewOpen = false;
	JFrame editor = new JFrame("Dialog Editor : " + Main.configuration.mobj.get(0));
	JFrame previewFrame = null;
	public Dialog dialog = new Dialog();
	public Dialog dialogCopy = new Dialog();
	JComboBox dropItemMobj = new JComboBox(getMobjList());
	JComboBox ifItemMobj = new JComboBox(getMobjList());
	JComboBox ifItem2Mobj = new JComboBox(getMobjList());
	JComboBox ifItem3Mobj = new JComboBox(getMobjList());
	JComboBox giveMobj = new JComboBox(getMobjList());
	JComboBox costMobj = new JComboBox(getMobjList());
	JComboBox altCostMobj = new JComboBox(getMobjList());
	JComboBox backdropPicker = new JComboBox();
	public JComboBox choicePicker = new JComboBox();
	JMenuBar menuBar = new JMenuBar();
	JMenu menu_file = new JMenu("File");
	JMenu menu_editor = new JMenu("Editor");
	JMenuItem[] menuItem = {
		new JMenuItem("New Script"),
		new JMenuItem("Open Script"),
		new JMenuItem("Save Script"),
		new JMenuItem("Exit"),
		new JMenuItem("Compile"),
		new JMenuItem("New NPC"),
		new JMenuItem("Change NPC"),
		new JMenuItem("Delete NPC"),
		new JMenuItem("Change Mobj"),
		new JMenuItem("Preview"),
		new JMenuItem("Load PWad"),
		new JMenuItem("Decompile"),
		new JMenuItem("Export USDF")
	};
	JTabbedPane tabs = new JTabbedPane(JTabbedPane.BOTTOM);
	JPanel preview = new JPanel(new BorderLayout());
	JPanel dialogPane = new JPanel(new BorderLayout());
	JPanel namePane = new JPanel(new FlowLayout(FlowLayout.LEFT));
	JPanel dialogSettings = new JPanel();
	JPanel choicesPanel = new JPanel(new BorderLayout());
	JPanel choicePickerPanel = new JPanel(new BorderLayout());
	JPanel choicePickerPanelButtons = new JPanel();
	JPanel choiceEditPanel = new JPanel();
	JTextArea dialogText = new JTextArea();
	JTextField nameField = new JTextField(8);
	JTextField voice = new JTextField(8);
	JTextField choiceText = new JTextField(16);
	JTextField yesText = new JTextField(16);
	JTextField noText = new JTextField(16);
	public JSpinner pageSpinner = new JSpinner();
	public JSpinner pageSpinner_ifItem = new JSpinner();
	public JSpinner pageSpinner_link = new JSpinner();
	JSpinner logSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 99999, 1));
	JSpinner costSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 65535, 1));
	JSpinner altCostSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 65535, 1));
	JCheckBox noDisplayCost = new JCheckBox();
	JCheckBox showNext = new JCheckBox(); //should the next page be shown immediately?
	JButton[] button = {
		new JButton("New"), //new page
		new JButton("Delete"), //delete page
		new JButton("Apply Settings"),
		new JButton("Apply Changes"), //noices
		new JButton("New"), //choices
		new JButton("Delete") //choices
	};
	private JFileChooser fc = new JFileChooser();

	public GUI()
	{
		editor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		editor.setSize(new Dimension(655, 502));
		editor.setMinimumSize(new Dimension(655, 502));
		editor.setJMenuBar(menuBar);
		editor.add(tabs);
		editor.add(new JLabel(Main.version, JLabel.RIGHT), BorderLayout.SOUTH);
		menuBar.add(menu_file);
		{
			menu_file.add(menuItem[0]);
			menuItem[0].addActionListener(this);
			menuItem[0].setActionCommand("New");
			menuItem[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
			menu_file.add(menuItem[1]);
			menuItem[1].addActionListener(this);
			menuItem[1].setActionCommand("Open");
			menuItem[1].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
			menu_file.add(menuItem[2]);
			menuItem[2].addActionListener(this);
			menuItem[2].setActionCommand("Save");
			menuItem[2].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
			menu_file.addSeparator();
			menu_file.add(menuItem[12]);
			menuItem[12].addActionListener(this);
			menuItem[12].setActionCommand("ExportUSDF");
			menu_file.addSeparator();
			menu_file.add(menuItem[4]);
			menuItem[4].addActionListener(this);
			menuItem[4].setActionCommand("Compile");
			menu_file.add(menuItem[11]);
			menuItem[11].addActionListener(this);
			menuItem[11].setActionCommand("Decompile");
			menu_file.addSeparator();
			menu_file.add(menuItem[10]);
			menuItem[10].addActionListener(this);
			menuItem[10].setActionCommand("LoadPWad");
			menu_file.addSeparator();
			menu_file.add(menuItem[3]);
			menuItem[3].addActionListener(this);
			menuItem[3].setActionCommand("Close");
			menuItem[3].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK));
		}
		menuBar.add(menu_editor);
		{
			menu_editor.add(menuItem[5]);
			menuItem[5].addActionListener(this);
			menuItem[5].setActionCommand("NewNPC");
			menu_editor.add(menuItem[6]);
			menuItem[6].addActionListener(this);
			menuItem[6].setActionCommand("ChangeNPC");
			menu_editor.add(menuItem[7]);
			menuItem[7].addActionListener(this);
			menuItem[7].setActionCommand("DeleteNPC");
			menu_editor.addSeparator();
			menu_editor.add(menuItem[8]);
			menuItem[8].addActionListener(this);
			menuItem[8].setActionCommand("PickMobj");
			menu_editor.addSeparator();
			menu_editor.add(menuItem[9]);
			menuItem[9].addActionListener(this);
			menuItem[9].setActionCommand("Preview");
		}
		tabs.addTab("Dialog", dialogPane);
		{
			dialogPane.add(namePane, BorderLayout.NORTH);
			namePane.add(new JLabel("Name:"));
			namePane.add(nameField);
			namePane.add(new JLabel("Page:"));
			namePane.add(pageSpinner);
			pageSpinner.addChangeListener(this);
			namePane.add(button[0]);
			button[0].addActionListener(this);
			button[0].setActionCommand("NewPage");
			namePane.add(button[1]);
			button[1].addActionListener(this);
			button[1].setActionCommand("DeletePage");
			nameField.addCaretListener(this);
			dialogPane.add(dialogText);
			dialogText.setLineWrap(true);
			dialogText.addCaretListener(this);
		}
		tabs.addTab("Page Settings", dialogSettings);
		{
			SpringLayout layout = new SpringLayout();
			dialogSettings.setLayout(layout);
			//Panel a.k.a. backdrop
			JLabel backdropLabel = new JLabel("Panel:");
			dialogSettings.add(backdropLabel);
			layout.putConstraint(SpringLayout.NORTH, backdropLabel, 5, SpringLayout.NORTH, dialogSettings);
			layout.putConstraint(SpringLayout.WEST, backdropLabel, 2, SpringLayout.WEST, dialogSettings);
			backdropPicker.setEditable(true);
			for(Lump lump : Main.iwad.directory) //look for any likely backdrop candidates
			{
				if(lump.name.indexOf("M_") == 0)
				{
					backdropPicker.addItem(lump.name);
				}
			}
			dialogSettings.add(backdropPicker);
			layout.putConstraint(SpringLayout.WEST, backdropPicker, 5, SpringLayout.EAST, backdropLabel);
			layout.putConstraint(SpringLayout.NORTH, backdropPicker, -3, SpringLayout.NORTH, backdropLabel);
			JLabel voiceLabel = new JLabel("Voice:");
			dialogSettings.add(voiceLabel);
			layout.putConstraint(SpringLayout.NORTH, voiceLabel, 10, SpringLayout.SOUTH, backdropLabel);
			layout.putConstraint(SpringLayout.WEST, voiceLabel, 0, SpringLayout.WEST, backdropLabel);
			dialogSettings.add(voice);
			layout.putConstraint(SpringLayout.NORTH, voice, 3, SpringLayout.SOUTH, backdropPicker);
			layout.putConstraint(SpringLayout.WEST, voice, 0, SpringLayout.WEST, backdropPicker);
			JLabel dropItem = new JLabel("Drop Item:");
			dialogSettings.add(dropItem);
			layout.putConstraint(SpringLayout.NORTH, dropItem, 10, SpringLayout.SOUTH, voiceLabel);
			layout.putConstraint(SpringLayout.WEST, dropItem, 0, SpringLayout.WEST, voiceLabel);
			dialogSettings.add(dropItemMobj);
			layout.putConstraint(SpringLayout.WEST, dropItemMobj, 5, SpringLayout.EAST, dropItem);
			layout.putConstraint(SpringLayout.NORTH, dropItemMobj, -3, SpringLayout.NORTH, dropItem);
			JLabel ifItem = new JLabel("If Item:");
			dialogSettings.add(ifItem);
			layout.putConstraint(SpringLayout.NORTH, ifItem, 10, SpringLayout.SOUTH, dropItem);
			layout.putConstraint(SpringLayout.WEST, ifItem, 0, SpringLayout.WEST, dropItem);
			dialogSettings.add(ifItemMobj);
			layout.putConstraint(SpringLayout.WEST, ifItemMobj, 0, SpringLayout.WEST, dropItemMobj);
			layout.putConstraint(SpringLayout.NORTH, ifItemMobj, -3, SpringLayout.NORTH, ifItem);
			JLabel ifItem2 = new JLabel("Or:"); //if Item2
			dialogSettings.add(ifItem2);
			layout.putConstraint(SpringLayout.NORTH, ifItem2, 10, SpringLayout.SOUTH, ifItem);
			layout.putConstraint(SpringLayout.EAST, ifItem2, 0, SpringLayout.EAST, ifItem);
			dialogSettings.add(ifItem2Mobj);
			layout.putConstraint(SpringLayout.WEST, ifItem2Mobj, 0, SpringLayout.WEST, ifItemMobj);
			layout.putConstraint(SpringLayout.NORTH, ifItem2Mobj, -3, SpringLayout.NORTH, ifItem2);
			JLabel ifItem3 = new JLabel("Or:"); //if Item3
			dialogSettings.add(ifItem3);
			layout.putConstraint(SpringLayout.NORTH, ifItem3, 10, SpringLayout.SOUTH, ifItem2);
			layout.putConstraint(SpringLayout.EAST, ifItem3, 0, SpringLayout.EAST, ifItem2);
			dialogSettings.add(ifItem3Mobj);
			layout.putConstraint(SpringLayout.WEST, ifItem3Mobj, 0, SpringLayout.WEST, ifItem2Mobj);
			layout.putConstraint(SpringLayout.NORTH, ifItem3Mobj, -3, SpringLayout.NORTH, ifItem3);
			JLabel gotoLabel = new JLabel("Go to:");
			dialogSettings.add(gotoLabel);
			layout.putConstraint(SpringLayout.NORTH, gotoLabel, 10, SpringLayout.SOUTH, ifItem3);
			layout.putConstraint(SpringLayout.WEST, gotoLabel, 15, SpringLayout.WEST, ifItem);
			dialogSettings.add(pageSpinner_ifItem);
			layout.putConstraint(SpringLayout.WEST, pageSpinner_ifItem, 5, SpringLayout.EAST, gotoLabel);
			layout.putConstraint(SpringLayout.NORTH, pageSpinner_ifItem, -3, SpringLayout.NORTH, gotoLabel);
			button[2].addActionListener(this);
			button[2].setActionCommand("ApplySettings");
			dialogSettings.add(button[2]);
			layout.putConstraint(SpringLayout.NORTH, button[2], 10, SpringLayout.SOUTH, gotoLabel);
			layout.putConstraint(SpringLayout.WEST, button[2], 0, SpringLayout.WEST, ifItem);
		}
		tabs.addTab("Choices", choicesPanel);
		{
			choicesPanel.add(choicePickerPanel, BorderLayout.NORTH);
			choicePickerPanel.add(choicePicker, BorderLayout.NORTH);
			choicePicker.addItemListener(this);
			choicePickerPanel.add(choicePickerPanelButtons);
			choicePickerPanelButtons.add(button[4]);
			button[4].addActionListener(this);
			button[4].setActionCommand("NewChoice");
			choicePickerPanelButtons.add(button[5]);
			button[5].addActionListener(this);
			button[5].setActionCommand("RemoveChoice");
			choicePickerPanelButtons.add(button[3]);
			button[3].addActionListener(this);
			button[3].setActionCommand("ApplyChoice");
			choicesPanel.add(choiceEditPanel);
			SpringLayout layout = new SpringLayout();
			choiceEditPanel.setLayout(layout);
			JLabel textLabel = new JLabel("Text:");
			choiceEditPanel.add(textLabel);
			layout.putConstraint(SpringLayout.NORTH, textLabel, 2, SpringLayout.NORTH, choiceEditPanel);
			layout.putConstraint(SpringLayout.WEST, textLabel, 2, SpringLayout.WEST, choiceEditPanel);
			choiceEditPanel.add(choiceText);
			layout.putConstraint(SpringLayout.NORTH, choiceText, -1, SpringLayout.NORTH, textLabel);
			layout.putConstraint(SpringLayout.WEST, choiceText, 5, SpringLayout.EAST, textLabel);
			JLabel yesLabel = new JLabel("Yes:");
			choiceEditPanel.add(yesLabel);
			layout.putConstraint(SpringLayout.NORTH, yesLabel, 5, SpringLayout.SOUTH, textLabel);
			layout.putConstraint(SpringLayout.WEST, yesLabel, 0, SpringLayout.WEST, textLabel);
			choiceEditPanel.add(yesText);
			layout.putConstraint(SpringLayout.NORTH, yesText, -1, SpringLayout.NORTH, yesLabel);
			layout.putConstraint(SpringLayout.WEST, yesText, 0, SpringLayout.WEST, choiceText);
			JLabel noLabel = new JLabel("No:");
			choiceEditPanel.add(noLabel);
			layout.putConstraint(SpringLayout.NORTH, noLabel, 5, SpringLayout.SOUTH, yesLabel);
			layout.putConstraint(SpringLayout.WEST, noLabel, 0, SpringLayout.WEST, yesLabel);
			choiceEditPanel.add(noText);
			layout.putConstraint(SpringLayout.NORTH, noText, -1, SpringLayout.NORTH, noLabel);
			layout.putConstraint(SpringLayout.WEST, noText, 0, SpringLayout.WEST, yesText);
			JLabel logLabel = new JLabel("Log Number:");
			choiceEditPanel.add(logLabel);
			layout.putConstraint(SpringLayout.NORTH, logLabel, 6, SpringLayout.SOUTH, noLabel);
			layout.putConstraint(SpringLayout.WEST, logLabel, 0, SpringLayout.WEST, noLabel);
			choiceEditPanel.add(logSpinner);
			layout.putConstraint(SpringLayout.NORTH, logSpinner, -1, SpringLayout.NORTH, logLabel);
			layout.putConstraint(SpringLayout.WEST, logSpinner, 5, SpringLayout.EAST, logLabel);
			JLabel logLabelNote = new JLabel("-1 means no log");
			choiceEditPanel.add(logLabelNote);
			layout.putConstraint(SpringLayout.NORTH, logLabelNote, 0, SpringLayout.NORTH, logLabel);
			layout.putConstraint(SpringLayout.WEST, logLabelNote, 5, SpringLayout.EAST, logSpinner);
			JLabel giveLabel = new JLabel("Give Item:");
			choiceEditPanel.add(giveLabel);
			layout.putConstraint(SpringLayout.NORTH, giveLabel, 6, SpringLayout.SOUTH, logLabel);
			layout.putConstraint(SpringLayout.WEST, giveLabel, 0, SpringLayout.WEST, logLabel);
			choiceEditPanel.add(giveMobj);
			layout.putConstraint(SpringLayout.NORTH, giveMobj, -1, SpringLayout.NORTH, giveLabel);
			layout.putConstraint(SpringLayout.WEST, giveMobj, 5, SpringLayout.EAST, giveLabel);
			JLabel costLabel = new JLabel("Cost:");
			choiceEditPanel.add(costLabel);
			layout.putConstraint(SpringLayout.NORTH, costLabel, 11, SpringLayout.SOUTH, giveLabel);
			layout.putConstraint(SpringLayout.WEST, costLabel, 0, SpringLayout.WEST, giveLabel);
			choiceEditPanel.add(costMobj);
			layout.putConstraint(SpringLayout.NORTH, costMobj, -2, SpringLayout.NORTH, costLabel);
			layout.putConstraint(SpringLayout.WEST, costMobj, 0, SpringLayout.WEST, giveMobj);
			JLabel amountLabel = new JLabel("Amount:");
			choiceEditPanel.add(amountLabel);
			layout.putConstraint(SpringLayout.NORTH, amountLabel, 0, SpringLayout.NORTH, costLabel);
			layout.putConstraint(SpringLayout.WEST, amountLabel, 5, SpringLayout.EAST, costMobj);
			choiceEditPanel.add(costSpinner);
			layout.putConstraint(SpringLayout.NORTH, costSpinner, -1, SpringLayout.NORTH, amountLabel);
			layout.putConstraint(SpringLayout.WEST, costSpinner, 5, SpringLayout.EAST, amountLabel);
			JLabel displayLabel = new JLabel("Display:");
			choiceEditPanel.add(displayLabel);
			layout.putConstraint(SpringLayout.NORTH, displayLabel, 0, SpringLayout.NORTH, amountLabel);
			layout.putConstraint(SpringLayout.WEST, displayLabel, 5, SpringLayout.EAST, costSpinner);
			choiceEditPanel.add(noDisplayCost);
			layout.putConstraint(SpringLayout.NORTH, noDisplayCost, -1, SpringLayout.NORTH, displayLabel);
			layout.putConstraint(SpringLayout.WEST, noDisplayCost, 5, SpringLayout.EAST, displayLabel);
			JLabel altCostLabel = new JLabel("Alt-Cost:");
			choiceEditPanel.add(altCostLabel);
			layout.putConstraint(SpringLayout.NORTH, altCostLabel, 11, SpringLayout.SOUTH, costLabel);
			layout.putConstraint(SpringLayout.WEST, altCostLabel, 0, SpringLayout.WEST, costLabel);
			choiceEditPanel.add(altCostMobj);
			layout.putConstraint(SpringLayout.NORTH, altCostMobj, -2, SpringLayout.NORTH, altCostLabel);
			layout.putConstraint(SpringLayout.WEST, altCostMobj, 0, SpringLayout.WEST, giveMobj);
			JLabel altAmountLabel = new JLabel("Amount:");
			choiceEditPanel.add(altAmountLabel);
			layout.putConstraint(SpringLayout.NORTH, altAmountLabel, 0, SpringLayout.NORTH, altCostLabel);
			layout.putConstraint(SpringLayout.WEST, altAmountLabel, 5, SpringLayout.EAST, altCostMobj);
			choiceEditPanel.add(altCostSpinner);
			layout.putConstraint(SpringLayout.NORTH, altCostSpinner, -1, SpringLayout.NORTH, altAmountLabel);
			layout.putConstraint(SpringLayout.WEST, altCostSpinner, 5, SpringLayout.EAST, altAmountLabel);
			JLabel linkLabel = new JLabel("Go to:");
			choiceEditPanel.add(linkLabel);
			layout.putConstraint(SpringLayout.NORTH, linkLabel, 10, SpringLayout.SOUTH, altCostLabel);
			layout.putConstraint(SpringLayout.WEST, linkLabel, 0, SpringLayout.WEST, altCostLabel);
			choiceEditPanel.add(pageSpinner_link);
			layout.putConstraint(SpringLayout.NORTH, pageSpinner_link, -1, SpringLayout.NORTH, linkLabel);
			layout.putConstraint(SpringLayout.WEST, pageSpinner_link, 5, SpringLayout.EAST, linkLabel);
			JLabel showNextLabel = new JLabel("Immediate:");
			choiceEditPanel.add(showNextLabel);
			layout.putConstraint(SpringLayout.NORTH, showNextLabel, 0, SpringLayout.NORTH, linkLabel);
			layout.putConstraint(SpringLayout.WEST, showNextLabel, 5, SpringLayout.EAST, pageSpinner_link);
			choiceEditPanel.add(showNext);
			layout.putConstraint(SpringLayout.NORTH, showNext, -1, SpringLayout.NORTH, showNextLabel);
			layout.putConstraint(SpringLayout.WEST, showNext, 5, SpringLayout.EAST, showNextLabel);
		}
		tabs.addTab("Preview", preview);
		{
			preview.add(dialogCopy);
		}
		editor.setVisible(true);
	}

	public void setPage(DialogBlock page)
	{
		dialogText.setText(page.getDialog());
		nameField.setText(page.getName());
		backdropPicker.setSelectedItem(page.getPanel());
		voice.setText(page.getVoice());
		dropItemMobj.setSelectedIndex(Main.page.drop+1);
		ifItemMobj.setSelectedIndex(Main.page.if_item[0]+1);
		ifItem2Mobj.setSelectedIndex(Main.page.if_item[1]+1);
		ifItem3Mobj.setSelectedIndex(Main.page.if_item[2]+1);
		pageSpinner_ifItem.getModel().setValue(Main.page.if_item_goto);
		choicePicker.setModel(new DefaultComboBoxModel(Main.page.choices.toArray()));
		if(Main.page.choices.size() != 0)
		{
			choicePicker.setSelectedIndex(0);
			setChoice(Main.page.choices.get(0));
		}
		else //We need to update the tab but to a non-existant choice
		{
			setChoice(new DialogChoice());
		}
	}
	public void setChoice(DialogChoice choice)
	{
		choiceText.setText(choice.getText());
		yesText.setText(choice.getYes());
		noText.setText(choice.getNo());
		logSpinner.getModel().setValue(choice.log);
		costSpinner.getModel().setValue(choice.cost_check);
		altCostSpinner.getModel().setValue(choice.altcost_check);
		giveMobj.setSelectedIndex(choice.give+1);
		costMobj.setSelectedIndex(choice.cost+1);
		altCostMobj.setSelectedIndex(choice.altcost+1);
		noDisplayCost.setSelected(!choice.noDisplayCost);
		showNext.setSelected(choice.showNext);
		pageSpinner_link.getModel().setValue(choice.link);
		if(previewOpen)
			dialog.repaint();
	}
	public void actionPerformed(ActionEvent e)
	{
		String cmd = e.getActionCommand();
		//modifiers (these execute something before executing a different command)
		if(cmd.equals("SaveAs"))
		{
			fc = new JFileChooser();
		}
		//commands
		if(cmd.equals("Close"))
		{
			System.exit(0);
		}
		else if(cmd.equals("NewPage"))
		{
			Main.script.NPCs.get(Main.script.activeNPC).newPage();
		}
		else if(cmd.equals("DeletePage"))
		{
			int index = (Integer) pageSpinner.getModel().getValue();
			for(int i = 0;i < Main.script.NPCs.get(Main.script.activeNPC).pages.size();i++)
			{
				for(int j = 0;j < Main.script.NPCs.get(Main.script.activeNPC).pages.get(i).choices.size();j++)
				{
					if(Main.script.NPCs.get(Main.script.activeNPC).pages.get(i).choices.get(j).link == index)
					{
						Main.script.NPCs.get(Main.script.activeNPC).pages.get(i).choices.get(j).link = -2;
					}
				}
			}
			Main.script.NPCs.get(Main.script.activeNPC).deletePage(Main.page);
			if(Main.page.choices.size() > 0)
			{
				setChoice(Main.page.choices.get(0));
			}
		}
		else if(cmd.equals("NewChoice"))
		{
			if(Main.page.choices.size() == 5) //max of 5 choices
			{
				return;
			}
			DialogChoice choice = new DialogChoice();
			choice.setText("New Choice");
			Main.page.choices.add(choice);
			setChoice(choice);
			choicePicker.addItem(choice);
			choicePicker.setSelectedIndex(Main.page.choices.size()-1);
		}
		else if(cmd.equals("RemoveChoice"))
		{
			if(Main.page.choices.size() == 0)
			{
				return;
			}
			DialogChoice choice = (DialogChoice) choicePicker.getSelectedItem();
			Main.page.choices.remove(choice);
			choicePicker.removeItemAt(choicePicker.getSelectedIndex());
			if(choicePicker.getSelectedIndex() > 0)
			{
				choicePicker.setSelectedIndex(choicePicker.getSelectedIndex() - 1);
			}
			else //clear choice editor
			{
				if(Main.page.choices.size() == 0)
				{
					setChoice(new DialogChoice());
				}
			}
		}
		else if(cmd.equals("NewNPC"))
		{
			if(Main.script.NPCs.size() == 256) //according to KSSC there is a limit of 256 npcs
			{
				return;
			}
			//find unused npc
			int nextUnused = 0;
			for(int i = 0;i < Main.script.NPCs.size();i++)
			{
				if(Main.script.NPCs.get(i).actor >= nextUnused)
				{
					i = 0;
					nextUnused++;
					if(nextUnused == Main.configuration.mobj.size())
					{
						Main.error("Out of NPCs");
						return;
					}
				}
			}
			MobjPicker.pickMobj(nextUnused, "New NPC", MobjPicker.NPC_NEW);
		}
		else if(cmd.equals("ChangeNPC"))
		{
			MobjPicker.pickMobj(Main.script.NPCs.get(Main.script.activeNPC).actor, "Change NPC", MobjPicker.NPC_CHANGE);
		}
		else if(cmd.equals("DeleteNPC"))
		{
			//can we delete?
			if(Main.script.NPCs.size() == 1)
			{
				return;
			}
			//delete
			Main.script.NPCs.remove(Main.script.activeNPC);
			//change active
			if(Main.script.activeNPC != 0)
			{
				Main.script.activeNPC--;
			}
			//set the page
			Main.setPage(Main.script.NPCs.get(Main.script.activeNPC).pages.get(0));
			//update the title
			updateTitle();
		}
		else if(cmd.equals("PickMobj"))
		{
			MobjPicker.pickMobj(Main.script.NPCs.get(Main.script.activeNPC).actor, "Change NPC Mobj", MobjPicker.NPC_ACTOR);
		}
		else if(cmd.equals("ApplySettings"))
		{
			Main.page.setPanel((String) backdropPicker.getSelectedItem());
			Main.updateBackdrop();
			Main.page.setVoice(voice.getText());
			Main.page.drop = dropItemMobj.getSelectedIndex()-1;
			Main.page.if_item[0] = ifItemMobj.getSelectedIndex()-1;
			Main.page.if_item[1] = ifItem2Mobj.getSelectedIndex()-1;
			Main.page.if_item[2] = ifItem3Mobj.getSelectedIndex()-1;
			Main.page.if_item_goto = (Integer) pageSpinner_ifItem.getModel().getValue();
		}
		else if(cmd.equals("ApplyChoice"))
		{
			if(Main.page.choices.size() == 0)
			{
				return;
			}
			DialogChoice choice = Main.page.choices.get(choicePicker.getSelectedIndex());
			choicePicker.repaint(); //update the choice picker
			choice.setText(choiceText.getText());
			choice.setYes(yesText.getText());
			choice.setNo(noText.getText());
			choice.give = giveMobj.getSelectedIndex()-1;
			choice.cost = costMobj.getSelectedIndex()-1;
			choice.cost_check = (Integer) costSpinner.getModel().getValue();
			choice.altcost = altCostMobj.getSelectedIndex()-1;
			choice.altcost_check = (Integer) altCostSpinner.getModel().getValue();
			choice.noDisplayCost = !noDisplayCost.isSelected();
			choice.link = (Integer) pageSpinner_link.getModel().getValue();
			choice.showNext = showNext.isSelected();
			if(previewOpen)
				dialog.repaint();
		}
		else if(cmd.equals("Save") || cmd.equals("Compile") || cmd.equals("ExportUSDF"))
		{
			if(fc.getSelectedFile() == null)
			{
				int returnValue = fc.showSaveDialog(editor);
				if(returnValue != JFileChooser.APPROVE_OPTION)
				{
					return;
				}
			}
			if(cmd.equals("ExportUSDF"))
				Main.exportUSDF(fc.getSelectedFile());
			else
				Main.saveScript(fc.getSelectedFile());
		}
		else if(cmd.equals("New"))
		{
			Object[] options = {"Yes", "No"};
			int returnValue = JOptionPane.showOptionDialog(editor, "Are you sure you want to create a new script?\nAny unsaved changes will be lost.", "New Script Confirmation", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options, options[0]);
			if(returnValue == 0) //Must have chosen yes.
			{
				fc = new JFileChooser();
				Main.script = Script.newScript();
			}
			updateTitle();
		}
		else if(cmd.equals("Open"))
		{
			Object[] options = {"Yes", "No"};
			int value = JOptionPane.showOptionDialog(editor, "Are you sure you want to open a new script?\nAny unsaved changes will be lost.", "Open Script Confirmation", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options, options[0]);
			if(value == 0) //Must have chosen yes.
			{
				int returnValue = fc.showOpenDialog(editor);
				if(returnValue == JFileChooser.APPROVE_OPTION)
				{
					Main.script = Main.readScript(fc.getSelectedFile());
					Main.setPage(Main.script.NPCs.get(0).pages.get(0));
					updateTitle();
				}
			}
		}
		else if(cmd.equals("Decompile"))
		{
			Object[] options = {"Yes", "No"};
			int value = JOptionPane.showOptionDialog(editor, "Are you sure you want to decompile a script?\nAny unsaved changes will be lost.", "Decompile Script Confirmation", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options, options[0]);
			if(value == 0) //Must have chosen yes.
			{
				int returnValue = fc.showOpenDialog(editor);
				if(returnValue == JFileChooser.APPROVE_OPTION)
				{
					Main.script = Main.decompile(fc.getSelectedFile());
					Main.setPage(Main.script.NPCs.get(0).pages.get(0));
					updateTitle();
				}
			}
		}
		else if(cmd.equals("Preview"))
		{
			previewFrame = new JFrame("Preview");
			previewFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			previewFrame.add(dialog);
			previewFrame.setSize(new Dimension(330, 233));
			previewFrame.setMinimumSize(new Dimension(330, 233));
			previewFrame.setVisible(true);
			previewFrame.addWindowListener(this);
			previewOpen = true;
		}
		else if(cmd.equals("LoadPWad"))
		{
			int returnValue = fc.showOpenDialog(editor);
			if(returnValue == JFileChooser.APPROVE_OPTION)
			{
				if(previewOpen)
				{
					previewFrame.dispose();
				}
				editor.dispose();
				String[] args = {fc.getSelectedFile().toString()};
				Main.main(args);
			}
		}
		//to compile the file must be saved so we save then compile
		if(cmd.equals("Compile"))
		{
			String compileTo = fc.getSelectedFile().toString();
			if(compileTo.lastIndexOf(".") != -1)
			{
				compileTo = compileTo.substring(0, compileTo.lastIndexOf("."));
			}
			compileTo += ".lmp";
			if(compileTo.equals(fc.getSelectedFile()))
			{
				compileTo = fc.getSelectedFile() + ".o";
			}
			Main.compile(compileTo);
		}
	}
	public void updateTitle()
	{
		editor.setTitle("Dialog Editor : " + Main.configuration.mobj.get(Main.script.NPCs.get(Main.script.activeNPC).actor));
	}
	public void caretUpdate(CaretEvent e)
	{
		JTextComponent source = (JTextComponent) e.getSource();
		if(previewOpen)
			dialog.repaint();
		if(source == dialogText)
		{
			Main.page.setDialog(source.getText());
		}
		else if(source == nameField)
		{
			Main.page.setName(source.getText());
		}
	}
	public void stateChanged(ChangeEvent e)
	{
		//basically change the page.
		Main.setPage(Main.script.NPCs.get(Main.script.activeNPC).pages.get(((Integer) pageSpinner.getModel().getValue()).intValue()));
	}
	public static String[] getMobjList()
	{
		Object[] mobjs = Main.configuration.mobj.toArray();
		String[] returnList = new String[mobjs.length+1];
		returnList[0] = "<No Object>";
		for(int i = 0;i < mobjs.length;i++)
		{
			returnList[i+1] = (String) mobjs[i];
		}
		return returnList;
	}
	public void itemStateChanged(ItemEvent e)
	{
		if(e.getStateChange() == ItemEvent.SELECTED)
		{
			if(Main.page.choices.size() != 0)
			{
				setChoice((DialogChoice) e.getItem());
			}
		}
	}
	public void windowClosed(WindowEvent e)
	{
		previewOpen = false;
	}
}