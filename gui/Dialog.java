package gui;

import main.*;
import wad.DoomImage;
import static main.Main.font;
import static main.Main.page;

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

public class Dialog extends JPanel
{
	private final int TRANSLATION_NONE = 0;
	private final int TRANSLATION_GREEN = 1;
	private final int TRANSLATION_WHITE = 2;
	private final int BORDER = 16;
	private final int FONT_HEIGHT = font.font[0][0].image.getHeight()+2;
	private DoomImage backdrop = null;
	Image backBuffer;
	Graphics bb;
	private final Color shadow = new Color(0, 0, 0, 127);

	public void addNotify()
	{
		super.addNotify();
		backBuffer = createImage(320, 200);
		bb = backBuffer.getGraphics();
	}
	public void paint(Graphics g)
	{
		if(page == null)
		{
			return;
		}
		bb.setColor(Color.WHITE);
		bb.fillRect(0, 0, 320, 200);
		if(backdrop != null)
		{
			bb.drawImage(backdrop.image, -backdrop.xoffset, -backdrop.yoffset, this);
		}
		else
		{
			bb.setColor(shadow);
			bb.fillRect(14, 14, 290, drawString(bb, page.getDialog(), BORDER+8, BORDER+16, TRANSLATION_NONE, 1).height+FONT_HEIGHT+10);
		}
		drawString(bb, page.getName(), BORDER, BORDER, TRANSLATION_WHITE);
		drawString(bb, page.getDialog(), BORDER+8, BORDER+16, TRANSLATION_NONE);
		//Time to start the options
		bb.setColor(shadow);
		bb.fillRect(BORDER+12, BORDER+122, 272, 3+((FONT_HEIGHT-1)*(page.choices.size()+1)));
		for(int i = 0;i < page.choices.size();i++)
		{
			drawString(bb, (i+1) + ".", BORDER+38-getWidth((i+1) + "."), BORDER+124+((FONT_HEIGHT-1)*i), TRANSLATION_WHITE);
			drawString(bb, page.choices.get(i).toString(), BORDER+52, BORDER+124+((FONT_HEIGHT-1)*i), TRANSLATION_GREEN, 2);
		}
		//now there is always the "thanks, bye" option (text varies)
		drawString(bb, (page.choices.size()+1) + ".", BORDER+38-getWidth((page.choices.size()+1) + "."), BORDER+124+((FONT_HEIGHT-1)*page.choices.size()), TRANSLATION_WHITE);
		drawString(bb, "Thanks, bye!", BORDER+52, BORDER+124+((FONT_HEIGHT-1)*page.choices.size()), TRANSLATION_GREEN);
		g.drawImage(backBuffer, 0, 0, getSize().width, getSize().height, 0, 0, 320, 200, Color.CYAN, this);
	}
	public int getWidth(String string)
	{
		int width = 0;
		char[] str = string.toUpperCase().toCharArray();
		for(char character : str)
		{
			width += font.font[0][character-33].image.getWidth();
		}
		return width;
	}
	public void drawString(Graphics g, String string, int x, int y, int translation)
	{
		drawString(g, string, x, y, translation, 0);
	}
	public Dimension drawString(Graphics g, String string, int x, int y, int translation, int flags)
	{
		boolean drawImage = true; //1 - don't draw image
		boolean wordWrap = true; //2 - no word wrap
		//get flags
		if(flags >= 2)
		{
			wordWrap = false;
			flags -= 2;
		}
		if(flags >= 1)
		{
			drawImage = false;
			flags -= 1;
		}
		//draw image
		string = string.toUpperCase();
		char[] str = string.toCharArray();
		int startingX = x;
		int startingY = y;
		int i = 0;
		for(char character : str)
		{
			if(character == 32) //space
			{
				if(x+5+BORDER >= 320-BORDER)
				{
					y += FONT_HEIGHT;
					x = startingX;
				}
				else
				{
					x += 5;
					String nextWord = string.substring(i+1); //from here down is all about proper word wraping
					if(nextWord.indexOf(" ") != -1)
					{
						nextWord = nextWord.substring(0, nextWord.indexOf(" "));
					}
					if(x+getWidth(nextWord) > 298 && wordWrap)
					{
						y += FONT_HEIGHT;
						x = startingX;
					}
				}
			}
			else if(character == 10) //newline
			{
				y += FONT_HEIGHT;
				x = startingX;
			}
			else
			{
				DoomImage draw = font.font[translation][character-33];
				if(x+draw.image.getWidth() > 298 && wordWrap)
				{
					y += FONT_HEIGHT;
					x = startingX;
				}
				if(drawImage)
					g.drawImage(draw.image, x - draw.xoffset, y - draw.yoffset, this);
				x += draw.image.getWidth();
			}
			i++;
		}
		return new Dimension(320, (y - startingY) + FONT_HEIGHT);
	}
	public void setBackdrop(short[] data)
	{
		int width = data[0] + data[1]*0x100;
		int height = data[2] + data[3]*0x100;
		int dataPos = 8;
		int pointers[] = new int[width];
		for(int i = 0;i < width;i++)
		{
			pointers[i] = data[dataPos] + data[dataPos+1]*0x100 + data[dataPos+2]*0x10000 + data[dataPos+3]*0x1000000;
			dataPos += 4;
		}
		BufferedImage returnImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = returnImage.createGraphics();
		int x = 0;
		while(dataPos < data.length)
		{
			if(data[dataPos] == 0xFF)
			{
				x++;
				if(x == width)
				{
					break;
				}
				dataPos = pointers[x];
				continue;
			}
			int y = data[dataPos];
			int length = data[dataPos+1];
			dataPos += 3; //skip over wasted byte
			for(int i = 0;i < length;i++)
			{
				int color = data[dataPos];
				g.setColor(Main.playpal.palette[color]);
				g.drawLine(x, y, x, y);
				y++;
				dataPos += 1;
			}
			dataPos += 1;
		}
		backdrop = new DoomImage(returnImage);
		backdrop.xoffset = data[4] + data[5]*0x100;
		if(backdrop.xoffset > 65215)
		{
			backdrop.xoffset = -(65536 - backdrop.xoffset);
		}
		backdrop.yoffset = data[6] + data[7]*0x100;
		if(backdrop.yoffset > 65215)
		{
			backdrop.yoffset = -(65536 - backdrop.yoffset);
		}
		repaint();
	}
	public void removeBackdrop()
	{
		backdrop = null;
		repaint();
	}
}