package gui;

import main.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MobjPicker implements ActionListener
{
	public static int lastSelectedMobj = 0;
	private static int type = 0;
	private static JFrame frame;
	private static JComboBox selecter = new JComboBox(Main.configuration.mobj.toArray());
	private static JPanel buttonPanel = new JPanel();
	private static JButton select = new JButton("Select");
	private static JButton cancel = new JButton("Cancel");
	private static MobjPicker actionListener = new MobjPicker();
	public static final int NPC_ACTOR = 0;
	public static final int NPC_NEW = 1;
	public static final int NPC_CHANGE = 2;

	static
	{
		select.addActionListener(actionListener);
		select.setActionCommand("select");
		cancel.addActionListener(actionListener);
		cancel.setActionCommand("cancel");
	}

	public static void pickMobj(int select, String title, int mobjType)
	{
		Object[] mobjs = Main.configuration.mobj.toArray();
		switch(mobjType)
		{
			case NPC_CHANGE:
				for(int i = 0;i < mobjs.length;i++)
				{
					boolean unused = true;
					for(NPC npc : Main.script.NPCs)
					{
						if(((String) mobjs[i]).equals(Main.configuration.mobj.get(npc.actor)))
						{
							unused = false;
						}
					}
					if(unused)
					{
						mobjs[i] = (Object) "<Unused>";
					}
				}
				break;
			case NPC_NEW:
				for(int i = 0;i < mobjs.length;i++)
				{
					for(NPC npc : Main.script.NPCs)
					{
						if(((String) mobjs[i]).equals(Main.configuration.mobj.get(npc.actor)))
						{
							mobjs[i] = (Object) "<Used>";
						}
					}
				}
				break;
		}
		selecter = new JComboBox(mobjs);
		selecter.setSelectedIndex(select);
		frame = new JFrame(title);
		frame.add(selecter);
		frame.add(buttonPanel, BorderLayout.SOUTH);
		buttonPanel.add(MobjPicker.select);
		buttonPanel.add(cancel);
		frame.pack();
		frame.setResizable(false);
		frame.setVisible(true);
		type = mobjType;
	}
	public void actionPerformed(ActionEvent e)
	{
		String cmd = e.getActionCommand();
		if(cmd.equals("select"))
		{
			lastSelectedMobj = selecter.getSelectedIndex();
			switch(type)
			{
				case NPC_ACTOR:
					Main.script.NPCs.get(Main.script.activeNPC).actor = lastSelectedMobj;
					Main.gui.updateTitle();
					break;
				case NPC_NEW:
					if(((String) selecter.getSelectedItem()).equals("<Used>"))
					{
						return;
					}
					NPC npc = NPC.newNPC();
					npc.actor = lastSelectedMobj;
					Main.script.NPCs.add(npc);
					Main.script.activeNPC = Main.script.NPCs.size()-1;
					Main.gui.updateTitle();
					break;
				case NPC_CHANGE:
					if(((String) selecter.getSelectedItem()).equals("<Unused>"))
					{
						return;
					}
					for(int i = 0;i < Main.script.NPCs.size();i++)
					{
						if(Main.script.NPCs.get(i).actor == lastSelectedMobj)
						{
							Main.script.activeNPC = i;
							Main.script.NPCs.get(Main.script.activeNPC).updateModels();
							Main.setPage(Main.script.NPCs.get(i).pages.get(0));
						}
					}
					Main.gui.updateTitle();
					break;
			}
		}
		frame.setVisible(false);
	}
}