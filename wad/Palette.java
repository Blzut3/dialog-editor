package wad;

import wad.*;
import main.*;

import java.awt.*;

public class Palette
{
	public Color[] palette;

	public Palette(short[] data)
	{
		readPlayPal(data);
	}

	public void readPlayPal(short[] data)
	{
		palette = new Color[256];
		for(int i = 0;i < 768;i += 3)
		{
			palette[i/3] = new Color(data[i], data[i+1], data[i+2]);
		}
	}
}