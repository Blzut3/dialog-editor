package wad;

import java.awt.image.BufferedImage;

public class DoomImage
{
	public BufferedImage image;
	public int xoffset;
	public int yoffset;

	public DoomImage(int x, int y)
	{
		xoffset = x;
		yoffset = y;
	}
	public DoomImage(BufferedImage img)
	{
		this(0, 0);
		image = img;
	}
	public DoomImage()
	{
		this(0, 0);
	}
}