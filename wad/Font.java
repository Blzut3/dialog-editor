package wad;

import main.*;
import wad.*;

import java.awt.*;
import java.awt.image.*;

public class Font
{
	private final int TRANSLATION_NONE = 0;
	private final int TRANSLATION_GREEN = 1;
	private final int TRANSLATION_WHITE = 2;
	private String fontName;
	public DoomImage[][] font = {new DoomImage[63], new DoomImage[63], new DoomImage[63]};

	public Font(String fontName)
	{
		this.fontName = fontName;
		Lump[] lumps = new Lump[63];
		for(int i = 0;i < 63;i++)
		{
			lumps[i] = Main.iwad.getLumpByName(fontName + "0" + new Integer(i+33).toString());
			if(lumps[i].name.equals("") == false)
			{
				font[TRANSLATION_NONE][i] = getImage(Main.iwad.getDataForLump(lumps[i]), TRANSLATION_NONE);
				font[TRANSLATION_GREEN][i] = getImage(Main.iwad.getDataForLump(lumps[i]), TRANSLATION_GREEN);
				font[TRANSLATION_WHITE][i] = getImage(Main.iwad.getDataForLump(lumps[i]), TRANSLATION_WHITE);
			}
			else
			{
				font[TRANSLATION_NONE][i] = new DoomImage(new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB));
				font[TRANSLATION_GREEN][i] = font[TRANSLATION_NONE][i];
				font[TRANSLATION_WHITE][i] = font[TRANSLATION_NONE][i];
			}
		}
	}

	public DoomImage getImage(short[] data, int translation)
	{
		int width = data[0] + data[1]*0x100;
		int height = data[2] + data[3]*0x100;
		int dataPos = 8;
		int pointers[] = new int[width];
		for(int i = 0;i < width;i++)
		{
			pointers[i] = data[dataPos] + data[dataPos+1]*0x100 + data[dataPos+2]*0x10000 + data[dataPos+3]*0x1000000;
			dataPos += 4;
		}
		BufferedImage returnImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = returnImage.createGraphics();
		int x = 0;
		while(dataPos < data.length)
		{
			if(data[dataPos] == 0xFF)
			{
				x++;
				if(x == width)
				{
					break;
				}
				dataPos = pointers[x];
				continue;
			}
			int y = data[dataPos];
			int length = data[dataPos+1];
			dataPos += 3; //skip over wasted byte
			for(int i = 0;i < length;i++)
			{
				int color = data[dataPos];
				switch(translation)
				{
					case TRANSLATION_GREEN:
						if(color >= Main.configuration.translation[0] && color <= Main.configuration.translation[1])
						{
							color = Main.configuration.translation_green[0] + (color - Main.configuration.translation[0]);
						}
						else if(color == Main.configuration.translation[2])
						{
							color = Main.configuration.translation_green[2];
						}
						break;
					case TRANSLATION_WHITE:
						if(color >= Main.configuration.translation[0] && color <= Main.configuration.translation[1])
						{
							color = Main.configuration.translation_white[0] + (color - Main.configuration.translation[0]);
						}
						else if(color == Main.configuration.translation[2])
						{
							color = Main.configuration.translation_white[2];
						}
						break;
					default:
						break;
				}
				g.setColor(Main.playpal.palette[color]);
				g.drawLine(x, y, x, y);
				y++;
				dataPos += 1;
			}
			dataPos += 1;
		}
		DoomImage returnObject = new DoomImage(returnImage);
		returnObject.xoffset = data[4] + data[5]*0x100;
		if(returnObject.xoffset > 65215)
		{
			returnObject.xoffset = -(65536 - returnObject.xoffset);
		}
		returnObject.yoffset = data[6] + data[7]*0x100;
		if(returnObject.yoffset > 65215)
		{
			returnObject.yoffset = -(65536 - returnObject.yoffset);
		}
		return returnObject;
	}
}