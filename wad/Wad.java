package wad;

import main.Main;
import wad.*;

import java.io.*;
import java.util.*;

public class Wad
{
	public String wadFile;
	public ArrayList<Lump> directory;
	public long directoryOffset = 0;
	public long numLumps = 0;

	public Wad(String wadFile)
	{
		this.wadFile = wadFile;
		try
		{
			if(new File(wadFile).exists() == false)
			{
				Main.fatalError("Could not find wad.");
			}
		}
		catch(Exception e)
		{
			Main.fatalError(e, "Wad read error.");
		}
		initWad();
	}

	public void initWad()
	{
		directory = new ArrayList<Lump>();
		try
		{
			FileInputStream in = new FileInputStream(wadFile);
			in.skip(4);
			numLumps = in.read() + in.read()*0x100 + in.read()*0x10000 + in.read()*0x1000000;
			directoryOffset = in.read() + in.read()*0x100 + in.read()*0x10000 + in.read()*0x1000000;
			in.skip(directoryOffset - 12);
			for(int i = 0;i < numLumps;i++)
			{
				long offset = in.read() + in.read()*0x100 + in.read()*0x10000 + in.read()*0x1000000;
				long size = in.read() + in.read()*0x100 + in.read()*0x10000 + in.read()*0x1000000;
				char[] name = {(char) in.read(), (char) in.read(), (char) in.read(), (char) in.read(), 
								(char) in.read(), (char) in.read(), (char) in.read(), (char) in.read()};
				directory.add(new Lump(size, offset, name));
			}
			in.close();
		}
		catch(Exception e)
		{
			Main.fatalError(e, "Wad read error.");
		}
	}
	public Lump getLumpByName(String name)
	{
		for(Lump lump : directory)
		{
			if(lump.name.equals(name))
			{
				return lump;
			}
		}
		return null;
	}
	public short[] getDataForLump(Lump lump)
	{
		return getDataForLump(lump, (int) lump.size);
	}
	public short[] getDataForLump(Lump lump, int length)
	{
		byte[] tempData = new byte[length];
		short[] returnData = new short[length];
		try
		{
			FileInputStream in = new FileInputStream(wadFile);
			in.skip((int) lump.offset);
			in.read(tempData, 0, length);
			in.close();
		}
		catch(Exception e)
		{
			Main.fatalError(e, "Wad read error.");
		}
		for(int i = 0;i < length;i++)
		{
			if(tempData[i] < 0)
			{
				returnData[i] = (short) (256 + tempData[i]);
			}
			else
			{
				returnData[i] = tempData[i];
			}
		}
		return returnData;
	}
}