package wad;

public class Lump
{
	public long size = 0;
	public long offset = 0;
	public String name = "";

	public Lump()
	{
		this(0, 0, "");
	}
	public Lump(long size, long offset, char[] name)
	{
		this(size, offset, new String(name));
	}
	public Lump(long size, long offset, String name)
	{
		this.size = size;
		this.offset = offset;
		this.name = name.trim();
	}

	public String toString()
	{
		return name;
	}
}